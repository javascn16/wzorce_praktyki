package patterns.behavioral.iterator;

import static java.lang.Integer.max;

/**
 * Dynamic array implementation that implements Iterable interface.
 * @param <T> is type of data stored in the array.
 */
public class IterableArray<T> implements Iterable<T> {
    private static final int defaultSize = 4;
    private int size = 0;
    private T[] array;

    /**
     * Default constructor.
     */
    public IterableArray() {
        array = (T[]) new Object[defaultSize];
    }

    /**
     * Constructor.
     * @param capacity defines how many elements array can store before it has to be resized.
     */
    public IterableArray(int capacity) {
        array = (T[]) new Object[max(4, capacity)];
    }

    @Override
    public Iterator<T> iterator() {
        return new ThisArrayIterator();
    }

    /**
     * Add item to the array.
     * @param item to be added.
     */
    public void add(T item) {
        if (size >= array.length) {
            resize();
        }
        array[size++] = item;
    }

    // Private method used to resize array when it cannot accumulate more elements.
    private void resize() {
        int newSize = array.length + array.length/2;
        T[] tmpArray = (T[]) new Object[newSize];
        for(int i = 0; i < array.length; i++) {
            tmpArray[i] = array[i];
        }
        array = tmpArray;
    }

    /**
     * Private implementations of iterator that is not accessible to outer world.
     * It is used via Iterator interface.
     */
    private class ThisArrayIterator implements Iterator<T> {
        int current = -1;

        @Override
        public boolean hasNext() {
            return (current + 1) < size;
        }

        @Override
        public T next() {
            if (hasNext()) {
                return array[++current];
            }
            return null;
        }

        @Override
        public void remove() {
            if (current == array.length && current >= 0) {
                return;
            }

            T[] tmpArray = (T[]) new Object[array.length - 1];
            for(int i = 0; i < current; i++) {
                tmpArray[i] = array[i];
            }
            for(int i = current + 1; i < array.length; i++) {
                tmpArray[i-1] = array[i];
            }
            array = tmpArray;
            --size;
            --current;
        }
    }
}
