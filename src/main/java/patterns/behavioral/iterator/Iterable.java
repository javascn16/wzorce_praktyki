package patterns.behavioral.iterator;

public interface Iterable<T> {
    Iterator<T> iterator();
}
