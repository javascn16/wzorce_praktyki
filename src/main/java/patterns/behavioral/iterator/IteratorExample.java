package patterns.behavioral.iterator;

public class IteratorExample {
    public static void main(String[] args) {
        IterableArray<Integer> myCollection = new IterableArray<>();
        for (int i = 0; i < 10; i++) {
            myCollection.add(i);
        }
        Iterator<Integer> iterator = myCollection.iterator();
        while(iterator.hasNext()) {
            Integer value = iterator.next();
            System.out.println(value);
        }
    }
}
