package patterns.behavioral.observer;

public class TemperatureReaderExample {
    private static TemperatureReader.TemperatureObserver temperatureObserver = new TemperatureReader.TemperatureObserver() {
        @Override
        public void onNewReading(float value) {
            System.out.println("New temperature reading (anon as var) " + value);
        }
    };
    public static void main(String[] args) {
        TemperatureReader reader = new TemperatureReader();
        reader.addObserver(value -> System.out.println("New temperature reading (lambda) " + value));
        reader.addObserver(new TemperatureReader.TemperatureObserver() {
            @Override
            public void onNewReading(float value) {
                System.out.println("New temperature reading (anon) " + value);
            }
        });
        reader.addObserver(temperatureObserver);

        reader.start();
    }
}
