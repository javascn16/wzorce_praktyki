package patterns.behavioral.observer;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static java.lang.Float.max;
import static java.lang.Float.min;

/**
 * Example of Observer pattern. TemperatureReader is an event source/generator and it keeps a list of observers that
 * want to get notified when there is a new temperature reading.
 */
public class TemperatureReader {

    /**
     * Observer interface.
     */
    public interface TemperatureObserver {
        void onNewReading(float value);
    }

    private final List<TemperatureObserver> observers = new ArrayList<>();

    private void notifyObservers(float value) {
        observers.forEach(temperatureObserver -> temperatureObserver.onNewReading(value));
    }

    public void addObserver(TemperatureObserver observer) {
        if (observer != null && !observers.contains(observer)) {
            observers.add(observer);
        }
    }

    public void start() {
        Runnable runnable = new Runnable() {
            int counter = 0;
            Random random = new Random();
            @Override
            public void run() {
                while (counter < 5) {
                    ++counter;
                    float temp = max(-45.0f, random.nextFloat()*10);
                    temp = min(temp, 100.0f);
                    notifyObservers(temp);
                    try {
                        int time = (int)min(2000, (int)max(500, random.nextFloat()*2000));
                        //System.out.println("Next reading in: " + time + " millisec");
                        Thread.sleep(time);
                    } catch (InterruptedException e) {

                    }
                }
            }
        };
        runnable.run();
    }
}
