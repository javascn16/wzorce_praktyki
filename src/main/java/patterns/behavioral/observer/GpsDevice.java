package patterns.behavioral.observer;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GpsDevice {
    private Scanner scanner;
    private List<GpsObserver> gpsObservers = new ArrayList<>();

    public GpsDevice() {
        scanner = new Scanner(System.in);
    }

    public interface GpsObserver {
        void onNewPosition(double latitude, double longitue);
    }

    public void addObserver(GpsObserver observer) {
        if (observer != null && !gpsObservers.contains(observer)) {
            gpsObservers.add(observer);
        }
    }

    public void start() {
        while(true) {
            System.out.println("New latitude:");
            double lat = Double.parseDouble(scanner.next());
            System.out.println("New longitude:");
            double lon = Double.parseDouble(scanner.next());
            notifyObservers(lat, lon);
        }
        // notifyObservers(newLatitude, newLongitude);
    }

    private void notifyObservers(double latitude, double longitude) {
        for (GpsObserver observer : gpsObservers) {
            observer.onNewPosition(latitude, longitude);
        }
    }
}
