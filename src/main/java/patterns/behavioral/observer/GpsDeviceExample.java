package patterns.behavioral.observer;

public class GpsDeviceExample {
    public static void main(String[] args) {
        GpsDevice gpsDevice = new GpsDevice();
        gpsDevice.addObserver(new GpsPositionObserver());
        gpsDevice.start();
    }
}
