package patterns.behavioral.observer;

public class GpsPositionObserver implements GpsDevice.GpsObserver {

    @Override
    public void onNewPosition(double latitude, double longitue) {
        System.out.println("Latitude: " + latitude + ", longitude: " + longitue);
    }
}
