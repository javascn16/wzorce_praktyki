package patterns.creational.builder;

public class BuilderExample {
    public static void main(String[] args) {
        AddressBuilder addressBuilder = new AddressBuilderImpl();
        Address address;
        addressBuilder.setAppartmentNumber(10)
                .setBuildingNumber(5)
                .setCity("Szczecin")
                .setStreet("Mickiewicza")
                .setCountry("Polska");

        //tutaj odpytujemy mape kodow pocztowych pod katem kodu
        String code = "70-222";
        address = addressBuilder
                .setCode(code)
                .build();

        System.out.println(address.toString());

        StringBuilder stringBuilder = new StringBuilder();
        System.out.println(stringBuilder.append("aaa")
                .append("bbb")
                .append("ccc")
                .toString());
    }
}
