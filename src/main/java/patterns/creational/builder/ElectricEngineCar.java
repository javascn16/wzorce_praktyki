package patterns.creational.builder;

public class ElectricEngineCar implements Car {
    EngineType engine = EngineType.Electric;
}
