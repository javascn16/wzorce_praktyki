package patterns.creational.builder;

public class Address {
    private String country;
    private String city;
    private String code;
    private String street;
    private int buildingNumber;
    private int appartmentNumber;

    Address(String country, String city, String code, String street, int buildingNumber, int appartmentNumber) {
        this.country = country;
        this.city = city;
        this.code = code;
        this.street = street;
        this.buildingNumber = buildingNumber;
        this.appartmentNumber = appartmentNumber;
    }

    @Override
    public String toString() {
        return country + ", " + code + " " + city + ", " + street + " " + buildingNumber
                + (appartmentNumber > 0 ? "/" + appartmentNumber : "");
    }
}

