package patterns.creational.builder;

public interface AddressBuilder {
    AddressBuilder setCountry(String country);

    AddressBuilder setCity(String city);

    AddressBuilder setCode(String code);

    AddressBuilder setStreet(String street);

    AddressBuilder setBuildingNumber(int number);

    AddressBuilder setAppartmentNumber(int number);

    Address build();
}
