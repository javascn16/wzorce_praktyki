package patterns.creational.builder;

public interface CarBuilder {
    CarBuilder setEngineType(Car.EngineType engineType);
    Car build();
}
