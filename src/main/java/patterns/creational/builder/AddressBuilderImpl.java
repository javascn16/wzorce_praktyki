package patterns.creational.builder;

public class AddressBuilderImpl implements AddressBuilder {
    private String country = "";
    private String city = "";
    private String code = "";
    private String street = "";
    private int buildingNumber = 0;
    private int appartmentNumber = 0;

    @Override
    public AddressBuilderImpl setCountry(String country) {
        this.country = country;
        return this;
    }
    @Override
    public AddressBuilderImpl setCity(String city) {
        this.city = city;
        return this;
    }
    @Override
    public AddressBuilderImpl setCode(String code) {
        this.code = code;
        return this;
    }
    @Override
    public AddressBuilderImpl setStreet(String street) {
        this.street = street;
        return this;
    }
    @Override
    public AddressBuilderImpl setBuildingNumber(int number) {
        this.buildingNumber = number;
        return this;
    }
    @Override
    public AddressBuilderImpl setAppartmentNumber(int number) {
        this.appartmentNumber = number;
        return this;
    }
    @Override
    public Address build() {
        return new Address(country, city, code, street, buildingNumber, appartmentNumber);
    }
}
