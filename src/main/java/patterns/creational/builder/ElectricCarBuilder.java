package patterns.creational.builder;

public class ElectricCarBuilder implements CarBuilder {

    Car.EngineType engine;

    @Override
    public CarBuilder setEngineType(Car.EngineType engineType) {
        engine = engineType;
        return this;
    }

    @Override
    public Car build() {
        return engine ==
                Car.EngineType.Electric ? new ElectricEngineCar() : null;
    }
}
