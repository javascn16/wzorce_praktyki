package patterns.creational.builder;

public interface Car {
    enum EngineType {
        Combustion,
        Electric
    }
}
