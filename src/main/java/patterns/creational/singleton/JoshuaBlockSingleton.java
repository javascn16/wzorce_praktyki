package patterns.creational.singleton;

/**
 * Singleton zaproponowany przez Joshua Bloch'a. Rozwiązuje problem dostępu do prywatnego konstruktora przez refleksję.
 * Wykorzystuje fakt, że Java zapewnia stworzenie stworzenie dokładnie jednej instancji każdej wartości enum-a.
 *
 * Wada:
 *      1) brak możliwości zastosowania leniwej inicjalizacji
 *      2) podczas serializacji tego singletona utracone zostają wartości jego pól
 */
public enum JoshuaBlockSingleton {
    instance;

    int val;

    public int getVal() {
        return val;
    }
}
