package patterns.creational.singleton;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileLog {
    private static final String logFile = "the_singleton_log.txt";
    private static int counter = 0;
    private static FileLog instance;

    private BufferedWriter writer;

    private FileLog() {
        try {
            writer = new BufferedWriter(new FileWriter(logFile, true));
            Date date = new Date(System.currentTimeMillis());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss z");
            writer.write("\n[" +simpleDateFormat.format(date) + "]" + ". Starting new log chunk:\n");
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException("Could not open log file for writing!");
        }
    }

    public static FileLog getInstance() {
        if (instance == null) {
            synchronized (FileLog.class) {
                if (instance == null) {
                    instance = new FileLog();
                }
            }
        }
        return instance;
    }

    public void log(String message) {
        try {
            writer.write(message + "\n");
            counter++;
            if (counter == 5) {
                writer.flush();
                counter = 0;
            }
        } catch (IOException e) {
            System.err.println("Failed to write log file!");
        }
    }

    public static void finish() {
        try {
            getInstance().writer.close();
        } catch (IOException e) {

        }
    }
}
