package patterns.creational.singleton;

/**
 * Singleton ze statycznym blokiem inicjalizacyjnym - podobny do zachłannego, ale umożliwia obsługę wyjątków.
 * Wada:
 *      1) podobnie jak zachłanny zostanie zainicjalizowany bez względu na to czy zostanie użyty
 *      2) możliwość uzyskania dostępu do konstruktora prywatnego przy użyciu refleksji
 */
public class StaticBlockInitSingleton {
    private StaticBlockInitSingleton() {}

    private static final StaticBlockInitSingleton instance;

    static {
        try {
            instance = new StaticBlockInitSingleton();
        } catch (Exception e) {
            throw new RuntimeException("Exception during singleton creation!");
        }
    }

    public static StaticBlockInitSingleton getInstance() {
        return instance;
    }
}
