package patterns.creational.singleton;

public class FileLogUser {
    public static void main(String[] args) {
        FileLog instance = FileLog.getInstance();
        instance.log("message 1");
        instance.log("message 2");
        instance.log("message 3");
        instance.log("message 4");
        instance.log("message 5");
        instance.log("message 6");

        FileLog instance_2 = FileLog.getInstance();
        instance_2.log("blaaa");
        FileLog.getInstance().log("msg");
        FileLog.finish();
    }
}
