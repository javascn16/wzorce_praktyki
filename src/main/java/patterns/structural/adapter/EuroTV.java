package patterns.structural.adapter;

public class EuroTV implements EuroDevice {
    private boolean isPlugged = false;
    @Override
    public void plugInEuro() {
        System.out.println("Plugging: " + EuroTV.class.getSimpleName());
        isPlugged = true;
    }

    @Override
    public void powerOn() {
        if (isPlugged) {
            System.out.println("Powered on: " + EuroTV.class.getSimpleName());
        } else {
            System.out.println("Please plug me in first!");
        }
    }
}
