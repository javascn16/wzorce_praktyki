package patterns.structural.adapter;

public class UnitedStatesTV implements UsDevice {
    private boolean isPlugged = false;
    @Override
    public void plugInUS() {
        System.out.println("Plugging: " + UnitedStatesTV.class.getSimpleName());
        isPlugged = true;
    }

    @Override
    public void powerOn() {
        if (isPlugged) {
            System.out.println("Powered on: " + UnitedStatesTV.class.getSimpleName());
        } else {
            System.out.println("Please plug me in first!");
        }
    }
}
