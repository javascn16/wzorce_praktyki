package patterns.structural.adapter;

public interface UsDevice {
    void plugInUS();
    void powerOn();
}
