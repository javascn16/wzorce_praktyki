package patterns.structural.adapter;

public interface EuroDevice {
    void plugInEuro();
    void powerOn();
}
