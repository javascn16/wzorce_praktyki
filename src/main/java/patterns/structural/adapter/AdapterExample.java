package patterns.structural.adapter;

public class AdapterExample {
    public static void main(String[] args) {
        UsDevice usTv = new UnitedStatesTV();
        UnitedStatesToEuroAdapter usTvAdapter = new UnitedStatesToEuroAdapter(usTv);
        EuroElectronicsConsumer consumer = new EuroElectronicsConsumer(usTvAdapter);
        consumer.use();
    }
}
