package patterns.structural.decorator;

public interface Car {
    void start();
    void turnLightsOn();
}
