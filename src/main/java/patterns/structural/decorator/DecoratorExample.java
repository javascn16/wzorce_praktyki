package patterns.structural.decorator;

public class DecoratorExample {
    public static void main(String[] args) {
        Car simpleCar = new SimpleCar();
        Car acCar = new AirConditionedCar(new SimpleCar());
        Car sportCar = new SportExhaustCarDecorator(new SimpleCar());
        Car sportyAcCar = new SportExhaustCarDecorator(new AirConditionedCar(new SimpleCar()));

        Car[] cars = { simpleCar, sportCar, acCar, sportyAcCar };
        for (Car car : cars) {
            car.start();
            System.out.println();
        }
    }
}
