package patterns.structural.decorator;

public abstract class CarDecorator implements Car {
    private final Car car;

    CarDecorator(Car theCar) {
        car = theCar;
    }

    @Override
    public void start() {
        car.start();
    }

    @Override
    public void turnLightsOn() {
        car.turnLightsOn();
    }
}
