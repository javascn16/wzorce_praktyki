package patterns.structural.proxy;

public class RealMovie implements Movie {

    @Override
    public void play() {
        System.out.println("Playing movie...");
    }

    @Override
    public void pause() {
        System.out.println("Pausing movie...");
    }

    @Override
    public void stop() {
        System.out.println("Stopping movie...");
    }
}
