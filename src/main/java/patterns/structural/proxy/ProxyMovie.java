package patterns.structural.proxy;

public class ProxyMovie implements Movie {
    private String userAccountName;
    private String movieTitle;
    boolean isPlaying = false;
    RealMovie movie;

    public ProxyMovie(String userAccountName, String movieTitle) {
        this.movieTitle = movieTitle;
        this.userAccountName = userAccountName;
    }
    @Override
    public void play() {
        if(getUserAge(userAccountName) >= getAgeRestriction(movieTitle)) {
            if (movie == null) {
                movie = new RealMovie();
            }
            isPlaying = true;
            movie.play();
        } else {
            System.out.println(userAccountName + ", you are not allowed to access this movie!");
        }
    }

    @Override
    public void pause() {
        if (isPlaying) {
            movie.pause();
        }
    }

    @Override
    public void stop() {
        if (isPlaying) {
            movie.stop();
        }
    }

    private int getUserAge(String accountName) {
        // reads user age from database
        if (accountName.toLowerCase().equals("Zosia".toLowerCase())) {
            return 4;
        } else {
            return 15;
        }
    }
    private int getAgeRestriction(String movieTitle) {
        // reads movie age rating from database
        return 6;
    }
}
