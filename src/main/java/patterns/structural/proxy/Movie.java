package patterns.structural.proxy;

public interface Movie {
    void play();
    void pause();
    void stop();
}
