package solid.s;

public class DatabaseImpl implements Database {
    private static DatabaseImpl database;

    boolean isOpen = false;

    private DatabaseImpl() {
    }

    public static Database getDatabase() {
        if (database == null) {
            database = new DatabaseImpl();
        }
        return database;
    }

    public void open() {
        if (!isOpen) {
            isOpen = true;
        }
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void close() {
        isOpen = false;
    }

    @Override
    public void executeQuery(String query) {

    }

    public void storeUser(String name, String address, String phoneNumber, int age) {
        if (isOpen) {
            //write user data
            System.out.println("Stored user:" + System.lineSeparator() +
                    "\tName        : " + name + System.lineSeparator() +
                    "\tAddress     : " + address + System.lineSeparator() +
                    "\tPhone Number: " + phoneNumber + System.lineSeparator() +
                    "\tAge         : " + age);
        }
    }

    public String getUserAddress(String name) {
        if (isOpen) {
            //read user data
            return "Some Street 11";
        }
        return null;
    }

    public String getUserPhoneNumber(String userName) {
        if (isOpen) {
            //read user phone number
            return "111 222 333";
        }
        return null;
    }

    public int getUserAge(String userName) {
        if (isOpen) {
            //read user age
            return 25;
        }
        return -1;
    }
}
