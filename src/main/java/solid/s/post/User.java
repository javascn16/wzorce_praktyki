package solid.s.post;

public class User {
    private String name;
    private String address;
    private String phoneNumber;
    private int age;

    public User() {
    }

    public User(String name, String address, String phoneNumber, int age) {
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.age = age;
    }

    void setUser(User in) {
        this.address = in.address;
        this.phoneNumber = in.phoneNumber;
        this.age = in.age;
        this.name = in.name;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Name: ");
        stringBuilder.append(name != null ? name : "");
        stringBuilder.append(", Address: ");
        stringBuilder.append(address != null ? address : "");
        stringBuilder.append(", Phone number: ");
        stringBuilder.append(phoneNumber != null ? phoneNumber : "");
        stringBuilder.append(", Age: ");
        stringBuilder.append(age >= 0 ? age : "");
        return stringBuilder.toString();
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public int getAge() {
        return age;
    }
}

