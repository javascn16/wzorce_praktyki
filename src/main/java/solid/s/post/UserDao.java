package solid.s.post;

public interface UserDao {
    User loadUser(String name);
    void storeUser(User user);
}
