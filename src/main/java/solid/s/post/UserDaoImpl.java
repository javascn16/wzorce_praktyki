package solid.s.post;

import solid.s.Database;

public class UserDaoImpl implements UserDao {
    private Database database;

    public UserDaoImpl(Database database) {
        this.database = database;
    }

    public User loadUser(String name) {
        if (!database.isOpen()) {
            database.open();
        }
        String address = database.getUserAddress(name);
        String number = database.getUserPhoneNumber(name);
        int age = database.getUserAge(name);
        return new User(name, address, number, age);
    }

    public void storeUser(User user) {
        if (!database.isOpen()) {
            database.open();
        }
        database.storeUser(user.getName(), user.getAddress(), user.getPhoneNumber(), user.getAge());
    }
}
