package solid.s.post;

import solid.s.DatabaseImpl;

public class SeparationOfConcernsExample {
    public static void main(String[] args) {
        User user = new User("Elmo", "Sesame Street 1", "123 456 789", 5);
        System.out.println(user.toString());
        UserDao userDao = new UserDaoImpl(DatabaseImpl.getDatabase());
        userDao.storeUser(user);
        user = userDao.loadUser("John");
        System.out.println(user.toString());
    }
}

