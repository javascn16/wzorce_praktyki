package solid.s.pre;

import solid.s.Database;
import solid.s.DatabaseImpl;

public class User {
    String name;
    String address;
    String phoneNumber;
    int age;

    public User() {
    }

    public User(String name, String address, String phoneNumber, int age) {
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.age = age;
    }

    void loadUser(String name) {
        Database db = DatabaseImpl.getDatabase();
        db.open();
        db.executeQuery("select * from users when user=" + name);
        if (db.isOpen()) {
            this.address = db.getUserAddress(name);
            this.phoneNumber = db.getUserPhoneNumber(name);
            this.age = db.getUserAge(name);
            this.name = name;
            db.close();
        }
    }

    void writeUser() {
        Database db = DatabaseImpl.getDatabase();
        db.open();
        if (db.isOpen()) {
            db.storeUser(name, address, phoneNumber, age);
            db.close();
        }
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Name: ");
        stringBuilder.append(name != null ? name : "");
        stringBuilder.append(", Address: ");
        stringBuilder.append(address != null ? address : "");
        stringBuilder.append(", Phone number: ");
        stringBuilder.append(phoneNumber != null ? phoneNumber : "");
        stringBuilder.append(", Age: ");
        stringBuilder.append(age >= 0 ? age : "");
        return stringBuilder.toString();
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public int getAge() {
        return age;
    }
}
