package solid.s.pre;

public class SeparationOfConcernsExample {
    public static void main(String[] args) {
        User user = new User("Elmo", "Sesame Street 1", "123 456 789", 5);
        System.out.println(user.toString());
        user.writeUser();
        user.loadUser("John");
        System.out.println(user.toString());
    }
}
