package solid.s;

public interface Database {
    void open();
    boolean isOpen();
    void close();
    void executeQuery(String query);
    void storeUser(String name, String address, String phoneNumber, int age);
    String getUserAddress(String userName);
    String getUserPhoneNumber(String userName);
    int getUserAge(String userName);
}
