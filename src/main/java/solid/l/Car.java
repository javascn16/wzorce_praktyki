package solid.l;

public interface Car {
    void start();
    void stop();
    void setLightsEnabled(boolean enabled);
    void changeTire(int tireNumber);
    void changeOil();
}
