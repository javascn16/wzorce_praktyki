package solid.l;

public class TeslaS implements Car {
    public void start() {

    }

    public void stop() {

    }

    public void setLightsEnabled(boolean enabled) {

    }

    public void changeTire(int tireNumber) {

    }

    public void changeOil() {
        // no oil to change in eletric car!
    }
}
