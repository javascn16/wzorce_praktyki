package solid.l;

public class Passat implements Car {
    public void start() {

    }

    public void stop() {

    }

    public void setLightsEnabled(boolean enabled) {

    }

    public void changeTire(int tireNumber) {

    }

    public void changeOil() {

    }
}
