package solid.o.post;

public interface Shape {
    double getArea();
}
