package solid.o.post;

public class Rectangle implements Shape {
    private int width;
    private int height;

    public Rectangle(int height, int width) {
        this.height = height;
        this.width = width;
    }

    public double getArea() {
        return width * height;
    }
}
