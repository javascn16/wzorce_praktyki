package solid.o.pre;

public class AreaCalculator {
    public double calculateArea(Object[] shapes) {
        double totalArea = 0.0;
        for (Object shape : shapes) {
            if (shape instanceof Rectangle) {
                Rectangle rectangle = (Rectangle) shape;
                totalArea += rectangle.width * rectangle.height;
            } else if (shape instanceof Circle) {
                Circle circle = (Circle) shape;
                totalArea += Math.pow(circle.radius, 2) * Math.PI;
            }
        }
        return totalArea;
    }
}
