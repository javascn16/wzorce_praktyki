package solid.i.pre;

public interface Animal {
    void eat();
    void rest();
    void fly();
    void swim();
    void run();
}
