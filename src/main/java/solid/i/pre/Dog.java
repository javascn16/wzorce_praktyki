package solid.i.pre;

public class Dog implements Animal {
    public void eat() {
        System.out.println("Dog is eating...");
    }

    public void rest() {
        System.out.println("Dog is reseting...");
    }

    public void fly() {
        // dogs dont fly!
    }

    public void swim() {
        System.out.println("Dog is swimming...");
    }

    public void run() {
        System.out.println("Dog is running...");
    }
}
