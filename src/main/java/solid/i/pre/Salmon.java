package solid.i.pre;

public class Salmon implements Animal {
    public void eat() {
        System.out.println("Salmon is eating...");
    }

    public void rest() {
        System.out.println("Salmon is resting...");
    }

    public void fly() {
        // salmons dont fly!
    }

    public void swim() {
        System.out.println("Salmon is swimming...");
    }

    public void run() {
        // salmons dont run!
    }
}
