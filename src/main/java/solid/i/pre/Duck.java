package solid.i.pre;

public class Duck implements Animal {
    public void eat() {
        System.out.println("Duck is eating...");
    }

    public void rest() {
        System.out.println("Duck is resting...");
    }

    public void fly() {
        System.out.println("Duck is flying...");
    }

    public void swim() {
        System.out.println("Duck is swimming...");
    }

    public void run() {
        System.out.println("Duck is running...");
    }
}
