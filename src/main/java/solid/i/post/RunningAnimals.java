package solid.i.post;

public interface RunningAnimals extends Animal {
    void run();
}
