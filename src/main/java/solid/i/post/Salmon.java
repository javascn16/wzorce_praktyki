package solid.i.post;

public class Salmon implements SwimmingAnimals {
    public void swim() {
        System.out.println("Salmon is swimming...");
    }

    public void eat() {
        System.out.println("Salmon is eating...");
    }

    public void rest() {
        System.out.println("Salmon is resting...");
    }
}
