package solid.i.post;

public class Duck implements Flying, RunningAnimals, SwimmingAnimals {
    public void fly() {
        System.out.println("Duck is flying...");
    }

    public void eat() {
        System.out.println("Duck is eating...");
    }

    public void rest() {
        System.out.println("Duck is resting...");
    }

    @Override
    public void run() {

    }

    @Override
    public void swim() {

    }
}
