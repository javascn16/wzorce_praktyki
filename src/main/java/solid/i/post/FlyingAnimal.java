package solid.i.post;

public interface FlyingAnimal extends Animal {
    void fly();
}
