package solid.i.post;

public class Dog implements RunningAnimals {
    public void run() {
        System.out.println("Dog is running...");
    }

    public void eat() {
        System.out.println("Dog is eating...");
    }

    public void rest() {
        System.out.println("Dog is resting...");
    }
}
