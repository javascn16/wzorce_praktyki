package solid.i.post;

public interface SwimmingAnimals extends Animal {
    void swim();
}
