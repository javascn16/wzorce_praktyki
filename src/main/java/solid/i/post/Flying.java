package solid.i.post;

public interface Flying {
    void fly();
}
