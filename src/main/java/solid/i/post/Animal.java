package solid.i.post;

public interface Animal {
    void eat();
    void rest();
}
