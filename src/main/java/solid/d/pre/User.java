package solid.d.pre;

import java.io.Console;

public class User {
    String name;
    int age;

    void display(Console console) {
        console.printf("%s, %d", name, age);
    }

    void display(View view) {
        view.setText(name + ", " + age);
    }
}
