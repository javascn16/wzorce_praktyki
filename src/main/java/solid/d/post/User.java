package solid.d.post;

public class User {
    String name;
    int age;
    UserPresenter presenter;

    public User(UserPresenter presenter) {
        this.presenter = presenter;
    }

    void display(UserPresenter presenter) {
        this.presenter.present(name, age);
    }
}