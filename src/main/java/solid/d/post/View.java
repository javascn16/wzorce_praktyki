package solid.d.post;

public interface View {
    void setText(String text);
}
