package solid.d.post;

public class UIViewPresenter implements UserPresenter {
    View userView;

    public void present(String name, int age) {
        userView.setText(name + ", " + age);
    }
}
