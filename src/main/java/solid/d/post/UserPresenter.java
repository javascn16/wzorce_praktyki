package solid.d.post;

public interface UserPresenter {
    void present(String name, int age);
}
