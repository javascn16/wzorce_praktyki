package solid.d.post;

import java.io.Console;

public class ConsolePresenter implements UserPresenter {
    public void present(String name, int age) {
        Console console = System.console();
        console.printf("%s, %d", name, age);
    }
}
