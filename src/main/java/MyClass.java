public class MyClass {
    private String text;

    public MyClass(String s) {
        text = s;
    }

    private MyClass() {
        text = "secret string";
    }

    boolean isSecret() {
        if (text.equals("secret string")) {
            System.out.println("a secret!");
            return true;
        }
        System.out.println("not a secret!");
        return false;
    }
}
